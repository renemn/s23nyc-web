export const imports = {
  'index.mdx': () =>
    import(/* webpackPrefetch: true, webpackChunkName: "index" */ 'index.mdx'),
  'components/UIButton/UIButton.mdx': () =>
    import(
      /* webpackPrefetch: true, webpackChunkName: "components-ui-button-ui-button" */ 'components/UIButton/UIButton.mdx'
    ),
}
