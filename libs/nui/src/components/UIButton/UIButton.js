import React from 'react';
import PropTypes from 'prop-types';

const UIButton = ({ type = 'button', children, onClick }) =>
	!children ? null : (
		<button type={type} className="ui-button" onClick={onClick}>
			{children}
		</button>
	);

UIButton.propTypes = {
	/** Type of button to display */
	type: PropTypes.string,
	/** Any component, string, number, etc */
	children: PropTypes.any,
	/** Function triggered when clicked */
	onClick: PropTypes.func
};

UIButton.defaultProps = {
	type: 'button',
	children: null,
	onClick: () => {}
};

export default UIButton;
