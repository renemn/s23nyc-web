import React from 'react';
import { mount, shallow } from 'enzyme';
import UIButton from './UIButton';

describe('Button Component', function() {
	it('Gets rendered when has children', function() {
		const wrapper = mount(
			<div className="example-01">
				<UIButton>
					<span>Example 01</span>
				</UIButton>
			</div>
		);
		expect(wrapper.find('.ui-button').length).toBe(1);
		expect(wrapper.isEmptyRender()).toBe(false);
	});

	it('Returns null when has not children', () => {
		const wrapper = mount(
			<div className="example-02">
				<UIButton />
			</div>
		);
		expect(wrapper.find('.ui-button').length).toBe(0);
		expect(wrapper.isEmptyRender()).toBe(true);
	});

	it('Handles onClick events', () => {
		const mockOnClick = jest.fn();
		const wrapper = mount(
			<div className="example-03">
				<UIButton onClick={mockOnClick}>
					<span>Example 03</span>
				</UIButton>
			</div>
		);
		wrapper.find('.ui-button').simulate('click');
		expect(wrapper.isEmptyRender()).toBe(false);
		expect(mockOnClick).toHaveBeenCalled();
	});
});
