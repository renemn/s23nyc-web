const path = require('path');
const cwd = process.cwd();

module.exports = {
	src: path.resolve(cwd, 'src'),
	host: '127.0.0.1',
	port: 3000
};
