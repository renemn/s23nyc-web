const path = require('path');
const babel = require('rollup-plugin-babel');
const commonjs = require('rollup-plugin-commonjs');
const { eslint } = require('rollup-plugin-eslint');
const json = require('rollup-plugin-json');
const nodeResolve = require('rollup-plugin-node-resolve');
const replace = require('rollup-plugin-replace');

const { getExternals } = require('../utils');

module.exports = ({ configPaths }) => {
	const paths = JSON.parse(configPaths);
	return {
		input: path.join(paths.src, 'index.js'),
		output: [
			{
				file: path.join(paths.lib, 'index.js'),
				format: 'cjs'
			},
			{
				file: path.join(paths.lib, 'index.es.js'),
				format: 'esm'
			}
		],
		external: getExternals(paths.package_json),
		plugins: [
			json(),
			eslint({
				configFile: paths.eslintrc,
				exclude: [`${paths.node_modules}/**`],
				include: [`${paths.src}/**`],
				throwOnError: true
			}),
			nodeResolve({
				extensions: ['.js', '.jsx'],
				module: true,
				jsnext: true
			}),
			replace({
				'process.env.NODE_ENV': JSON.stringify(
					process.env.NODE_ENV || 'development'
				)
			}),
			commonjs({
				include: [`${paths.node_modules}/**`]
			}),
			babel({
				exclude: `${paths.node_modules}/**`,
				babelrc: false,
				configFile: paths.babelrc,
				runtimeHelpers: true
			})
		]
	};
};
