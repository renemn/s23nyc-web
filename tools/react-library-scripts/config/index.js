const path = require('path');
const fs = require('fs-extra');

const pathsJson = require('./paths.json');
const config = module.exports;

config.setup = (options = {}) =>
	new Promise(async (resolve, reject) => {
		try {
			config.cwd = process.cwd();

			Object.keys(options)
				.filter(k => config.hasOwnProperty(k))
				.forEach(k => {
					config[k] =
						typeof options[k] === 'undefined'
							? config[k]
							: options[k];
				});

			config.paths = Object.keys(pathsJson).reduce((acc, curr) => {
				acc[curr] = path.resolve(config.cwd, pathsJson[curr]);
				return acc;
			}, {});

			config.paths.rolluprc = path.resolve(__dirname, 'rollup.config.js');
			config.paths.eslintrc = path.resolve(__dirname, '.eslintrc');
			config.paths.babelrc = path.resolve(__dirname, '.babelrc');
			config.paths.jestrc = path.resolve(__dirname, 'jest.js');
			config.paths.doczrc = path.resolve(__dirname, 'doczrc.js');

			config.package = await fs.readJson(config.paths.package_json);

			return resolve(config);
		} catch (err) {
			return reject(err);
		}
	});
