const path = require('path');
const pnpResolver = require.resolve('jest-pnp-resolver');

const cwd = process.cwd();

module.exports = {
	rootDir: cwd,
	verbose: true,
	collectCoverageFrom: [
		'src/**/*.{js,jsx}',
		'!src/**/*.stor(y|ies).{js,jsx}'
	],
	resolver: pnpResolver,
	setupFiles: [path.join(__dirname, 'jest/setup.js')],
	testMatch: [
		'<rootDir>/src/**/__tests__/**/*.{js,jsx}',
		'<rootDir>/src/**/?(*.)(spec|test).{js,jsx}'
	],
	testEnvironment: 'jsdom',
	testURL: 'http://localhost',
	transform: {
		'^.+\\.(js|jsx)$': path.resolve(__dirname, './jest/babelTransform.js'),
		'^.+\\.css$': path.resolve(__dirname, './jest/cssTransform.js'),
		'^(?!.*\\.(js|jsx|css|json)$)': path.resolve(
			__dirname,
			'jest/fileTransform.js'
		)
	},
	transformIgnorePatterns: ['[/\\\\]node_modules[/\\\\].+\\.(js|jsx)$'],
	moduleFileExtensions: [
		'web.js',
		'mjs',
		'js',
		'json',
		'web.jsx',
		'jsx',
		'node'
	]
};
