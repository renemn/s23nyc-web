require('@babel/register');
require('@babel/polyfill');
require('raf').polyfill(global);

const { configure } = require('enzyme');
const Adapter = require('enzyme-adapter-react-16');

configure({ adapter: new Adapter() });
