const { spawn } = require('child_process');

const config = require('../config');
const { handleChildProcess, getBin } = require('./utils');

const scripts = module.exports;

scripts.build = async function build(opts) {
	process.env.BABEL_ENV = 'production';
	process.env.NODE_ENV = 'production';
	try {
		await config.setup(opts);
		await handleChildProcess(
			spawn(
				getBin('rollup'),
				[
					'-c',
					config.paths.rolluprc,
					'--configPaths',
					JSON.stringify(config.paths)
				],
				{ stdio: ['pipe', 'inherit', 'pipe'] }
			)
		);
		process.exit(0);
	} catch (err) {
		if (err) console.log(err);
		process.exit(1);
	}
};

scripts.start = async function start(opts) {
	process.env.BABEL_ENV = 'development';
	process.env.NODE_ENV = 'development';
	try {
		await config.setup(opts);
		await handleChildProcess(
			spawn(getBin('docz'), ['dev', '--config', config.paths.doczrc], {
				stdio: 'inherit'
			})
		);
		process.exit(0);
	} catch (err) {
		if (err) console.log(err);
		process.exit(1);
	}
};

scripts.test = async function test(opts) {
	process.env.BABEL_ENV = 'test';
	process.env.NODE_ENV = 'test';
	try {
		await config.setup(opts);
		await handleChildProcess(
			spawn(getBin('jest'), ['-c', config.paths.jestrc, '--coverage'], {
				stdio: 'inherit'
			})
		);
		process.exit(0);
	} catch (err) {
		if (err) console.log(err);
		process.exit(1);
	}
};
