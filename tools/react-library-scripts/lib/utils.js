const path = require('path');

const fs = require('fs-extra');

const utils = module.exports;

const nodeModules = [
	'assert',
	'buffer',
	'child_process',
	'cluster',
	'console',
	'constants',
	'crypto',
	'dgram',
	'dns',
	'domain',
	'events',
	'fs',
	'http',
	'https',
	'module',
	'net',
	'os',
	'path',
	'process',
	'punycode',
	'querystring',
	'readline',
	'repl',
	'stream',
	'string_decoder',
	'timers',
	'tls',
	'tty',
	'url',
	'util',
	'v8',
	'vm',
	'zlib'
];

utils.getBin = function getBin(name) {
	const binPath = path.resolve(__dirname, `../node_modules/.bin/${name}`);
	if (fs.existsSync(binPath)) {
		return binPath;
	}
	return path.resolve(`node_modules/.bin/${name}`);
};

utils.getExternals = function getExternals(packagePath) {
	const pkg = JSON.parse(fs.readFileSync(packagePath, 'utf-8'));
	return [].concat.apply(
		[],
		[nodeModules, Object.keys(pkg.peerDependencies)]
	);
};

utils.handleChildProcess = function handleChildProcess(childProcess) {
	return new Promise(function childProcessEvents(resolve, reject) {
		childProcess.on('error', e => {
			reject(e);
		});

		childProcess.on('exit', code => {
			if (code === 0) resolve();
			else reject(`exit with code ${code}`);
		});
	});
};
