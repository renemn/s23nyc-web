#!/usr/bin/env node
'use strict';

var semver = process.versions.node.split('.');

if (semver[0] < 8) {
	throw new Error('react-library-scripts requires at least node v8.');
}

require('./react-library-scripts');
