const chalk = require('chalk');
const cli = require('commander');

const { version } = require('../package.json');
const scripts = require('../lib/scripts');

cli.version(version).usage(
	`react-library-scripts ${chalk.green('<command>')} [options]`
);

cli.command('build')
	.description('Creates JS packages ready to deploy')
	.action(scripts.build);

cli.command('start')
	.description('Runs docz as development server')
	.action(scripts.start);

cli.command('test')
	.description('Uses jest to test js scripts')
	.action(scripts.test);

cli.parse(process.argv);
